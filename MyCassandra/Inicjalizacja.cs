﻿using Cassandra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCassandra
{
    public class Inicjalizacja
    {
        public void CreateNamespace(Cluster cluster, String nazwaPrzestrzeni)
        {
            try
            {
                var session = cluster.Connect();
                session.Execute("CREATE KEYSPACE " + nazwaPrzestrzeni + " WITH replication " + "= {'class':'SimpleStrategy', 'replication_factor':1}; ");
            }
            catch (Exception e)
            {
                Console.WriteLine("WARNING: " + e.Message);
            }
        }

        public void CreateTableOferta(ISession session)
        {
            try
            {
                session.Execute("CREATE TABLE Oferta ( Id UUID PRIMARY KEY, Nazwa text, Panstwo text, Miejscowosc text, DniPobytu int, LiczbaOfert int );");
                session.Execute("INSERT INTO Oferta (Id, Nazwa, Panstwo, Miejscowosc, DniPobytu, LiczbaOfert) VALUES (uuid(), 'Italy All Inclusive', 'Włochy', 'Rzym', 5, 2)");
                session.Execute("INSERT INTO Oferta (Id, Nazwa, Panstwo, Miejscowosc, DniPobytu, LiczbaOfert) VALUES (uuid(), 'France All Inclusive', 'Francja', 'Paryż', 10, 2)");
            }
            catch (Exception e)
            {
                Console.WriteLine("WARNING: " + e.Message);
            }
        }

        public void CreateTableKlient(ISession session)
        {
            try
            {
                session.Execute("CREATE TABLE Klient ( Id UUID PRIMARY KEY, Imie text, Nazwisko text );");
                session.Execute("INSERT INTO Klient (Id, Imie, Nazwisko) VALUES (uuid(), 'Michał', 'Kowal' )");
                session.Execute("INSERT INTO Klient (Id, Imie, Nazwisko) VALUES (uuid(), 'Karol', 'Pawlik' )");
                
            }
            catch (Exception e)
            {
                Console.WriteLine("WARNING: " + e.Message);
            }
        }

        public void CreateTableOfertaKlient(ISession session)
        {
            try
            {
                session.Execute("CREATE TABLE OfertaKlient ( Id UUID PRIMARY KEY, IdOferta UUID, IdKlient UUID, Rozpoczecie timestamp, Zakonczenie timestamp );");
            }
            catch (Exception e)
            {
                Console.WriteLine("WARNING: " + e.Message);
            }
        }

        public void DropTables(ISession session)
        {
            session.Execute("DROP TABLE OfertaKlient;");
            session.Execute("DROP TABLE Oferta;");
            session.Execute("DROP TABLE Klient;");
        }
    }
}
