﻿using Cassandra;
using Cassandra.Data.Linq;
using Cassandra.Mapping;
using MyCassandra.Entity;
using MyCassandra.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCassandra
{
    class Program
    {
        public static void MenuGlowne()
        {
            Console.WriteLine("MENU GŁÓWNE");
            Console.WriteLine("1. Oferty");
            Console.WriteLine("2. Klienci");
            Console.WriteLine("3. Obsługa klienta");
            Console.WriteLine("4. Usuń tabele");
            Console.WriteLine("q. Wyjście");
        }
        public static void MenuOferta()
        {
            Console.WriteLine("MENU Oferty");
            Console.WriteLine("1. Dodaj");
            Console.WriteLine("2. Pokaż wszystkie");
            Console.WriteLine("3. Usuń");
            Console.WriteLine("4. Szukaj");
            Console.WriteLine("q. Wyjście");
        }
        public static void MenuKlient()
        {
            Console.WriteLine("MENU Klient");
            Console.WriteLine("1. Dodaj");
            Console.WriteLine("2. Pokaż wszystkie");
            Console.WriteLine("3. Edytuj");
            Console.WriteLine("4. Usuń");
            Console.WriteLine("q. Wyjście");
        }
        public static void MenuWypozyczenia()
        {
            Console.WriteLine("MENU Obsługi klienta");
            Console.WriteLine("1. Sprzedaj ofertę");
            Console.WriteLine("2. Historia ofert klienta");
            Console.WriteLine("3. Dostępne oferty");
            Console.WriteLine("q. Wyjście");
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Łączenie...");
            var cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
            Console.WriteLine("Tworzenie przestrzeni biuro_turystyczne...");
            Inicjalizacja inicjalizacja = new Inicjalizacja();
            inicjalizacja.CreateNamespace(cluster, "biuro_turystyczne");
            var session = cluster.Connect("biuro_turystyczne");
            Console.WriteLine("Tworzenie tabel i wstępnych danych...");
            inicjalizacja.CreateTableOferta(session);
            inicjalizacja.CreateTableKlient(session);
            inicjalizacja.CreateTableOfertaKlient(session);
            Console.WriteLine("Zakończono inicjalizację.");

            MappingConfiguration.Global.Define<MyMappings>();
            ConsoleKey key = new ConsoleKey();
            KlientRepo klientRepo = new KlientRepo(session);
            OfertaRepo ofertaRepo = new OfertaRepo(session);
            OfertaKlientRepo ofertaKlientRepo = new OfertaKlientRepo(session);

            while (true)
            {
                MenuGlowne();
                key = Console.ReadKey().Key;
                if (key.Equals(ConsoleKey.D1))
                {
                    while (true)
                    {
                        MenuOferta();
                        key = Console.ReadKey().Key;
                        if (key.Equals(ConsoleKey.D1))
                        {
                            Console.WriteLine("Podaj nazwe: ");
                            String nazwa = Console.ReadLine();
                            Console.WriteLine("Podaj Państwo: ");
                            String panstwo = Console.ReadLine();
                            Console.WriteLine("Podaj Miejscowosc: ");
                            String miejscowosc = Console.ReadLine();
                            Console.WriteLine("Podaj Ilość dni pobytu: ");
                            String dni = Console.ReadLine();
                            Console.WriteLine("Podaj liczbe ofert: ");
                            String liczbaOfert = Console.ReadLine();
                            Oferta oferta = new Oferta(nazwa, panstwo, miejscowosc, int.Parse(dni), int.Parse(liczbaOfert));
                            if (ofertaRepo.Save(oferta) == true)
                                Console.WriteLine("Zapisano !");
                            else
                                Console.WriteLine("Błąd zapisu");
                        }
                        else if (key.Equals(ConsoleKey.D2))
                        {
                            List<Oferta> oferty = ofertaRepo.FindAll();
                            foreach (Oferta oferta in oferty)
                            {
                                string dostepna = "";
                                if (oferta.LiczbaOfert > 0)
                                    dostepna = "dostępna";
                                else
                                    dostepna = "niedostępna";
                                Console.WriteLine(oferta.Id + " --- " + oferta.Nazwa + " - " + oferta.Panstwo + " - " + oferta.Miejscowosc + " - " + oferta.DniPobytu.ToString() + " (" + dostepna + ") " + oferta.LiczbaOfert);
                            }
                        }
                        else if (key.Equals(ConsoleKey.D3))
                        {
                            Console.WriteLine("Podaj ID oferty do usuniecia: ");
                            List<Oferta> oferty = ofertaRepo.FindAll();
                            foreach (Oferta oferta in oferty)
                            {
                                string dostepna = "";
                                if (oferta.LiczbaOfert > 0)
                                    dostepna = "dostępna";
                                else
                                    dostepna = "niedostępna";
                                Console.WriteLine(oferta.Id + " --- " + oferta.Nazwa + " - " + oferta.Panstwo + " - " + oferta.Miejscowosc + "-" + oferta.DniPobytu + " (" + dostepna + ")");
                            }
                            String id = Console.ReadLine();
                            if (ofertaRepo.FindOne(id) == null)
                                Console.WriteLine("Błędny numer ID");
                            else
                            {
                                if (ofertaRepo.Remove(id) == true)
                                    Console.WriteLine("Usunięto !");
                                else
                                    Console.WriteLine("Błąd usuwania");
                            }
                        }
                        else if (key.Equals(ConsoleKey.D4))
                        {
                            Console.WriteLine("Podaj Państwo: ");
                            String panstwo = Console.ReadLine();
                            List<Oferta> oferty = new List<Oferta>();
                            oferty = ofertaRepo.FindAll(panstwo);

                            if (oferty.Count != 0)
                            {
                                foreach (Oferta oferta in oferty)
                                {
                                    string dostepna = "";
                                    if (oferta.LiczbaOfert > 0)
                                        dostepna = "dostępna";
                                    else
                                        dostepna = "niedostępna";
                                    Console.WriteLine(oferta.Id + " --- " + oferta.Nazwa + " - " + oferta.Panstwo + " - " + oferta.Miejscowosc + "-" + oferta.DniPobytu + "  (" + dostepna + ")");
                                }
                            }
                            else
                                Console.WriteLine("Nie znaleziono ofert.");
                        }
                        else if (key.Equals(ConsoleKey.Q))
                        {
                            break;
                        }
                    }
                }
                else if (key.Equals(ConsoleKey.D2))
                {
                    while (true)
                    {
                        MenuKlient();
                        key = Console.ReadKey().Key;
                        if (key.Equals(ConsoleKey.D1))
                        {
                            Console.WriteLine("Podaj Imie: ");
                            String imie = Console.ReadLine();
                            Console.WriteLine("Podaj Nazwisko: ");
                            String nazwisko = Console.ReadLine();
                            Klient klient = new Klient(imie, nazwisko);
                            if (klientRepo.Save(klient) == true)
                                Console.WriteLine("Zapisano !");
                            else
                                Console.WriteLine("Błąd zapisu");
                        }
                        else if (key.Equals(ConsoleKey.D2))
                        {
                            List<Klient> klienci = klientRepo.FindAll();
                            foreach (Klient klient in klienci)
                            {
                                Console.WriteLine(klient.Id + " --- " + klient.Imie + " - " + klient.Nazwisko);
                            }
                        }
                        else if (key.Equals(ConsoleKey.D3))
                        {
                            Console.WriteLine("Podaj ID klienta do edycji: ");
                            List<Klient> klienci = klientRepo.FindAll();
                            foreach (Klient kl in klienci)
                            {
                                Console.WriteLine(kl.Id + " --- " + kl.Imie + " - " + kl.Nazwisko);
                            }
                            String id = Console.ReadLine();
                            Klient klient = klientRepo.FindOne(id);
                            if (klient == null)
                                Console.WriteLine("Błędny numer ID");
                            else
                            {
                                Console.WriteLine("Podawaj tylko dane które chcesz zmienić: ");
                                Console.WriteLine("Podaj imie: ");
                                String imie = Console.ReadLine();
                                Console.WriteLine("Podaj nazwisko: ");
                                String nazwisko = Console.ReadLine();
                                if(imie != "")
                                {
                                    klient.Imie = imie;
                                }
                                if(nazwisko != "")
                                {
                                    klient.Nazwisko = nazwisko;
                                }
                                if (klientRepo.Update(klient) == true)
                                    Console.WriteLine("Zapisano !");
                                else
                                    Console.WriteLine("Błąd zapisu");
                            }
                        }
                        else if (key.Equals(ConsoleKey.D4))
                        {
                            Console.WriteLine("Podaj ID klienta do usuniecia: ");
                            List<Klient> klienci = klientRepo.FindAll();
                            foreach (Klient klient in klienci)
                            {
                                Console.WriteLine(klient.Id + " --- " + klient.Imie + " - " + klient.Nazwisko);
                            }
                            String id = Console.ReadLine();
                            if (klientRepo.FindOne(id) == null)
                                Console.WriteLine("Błędny numer ID");
                            else
                            {
                                if (klientRepo.Remove(id) == true)
                                    Console.WriteLine("Usunięto !");
                                else
                                    Console.WriteLine("Błąd usuwania");
                            }
                        }
                        else if (key.Equals(ConsoleKey.Q))
                        {
                            break;
                        }
                    }
                }
                else if (key.Equals(ConsoleKey.D3))
                {
                    while (true)
                    {
                        MenuWypozyczenia();
                        key = Console.ReadKey().Key;
                        if (key.Equals(ConsoleKey.D1))
                        {
                            Console.WriteLine("Podaj ID klienta który kupuje oferte: ");
                            List<Klient> klienci = klientRepo.FindAll();
                            foreach (Klient kli in klienci)
                            {
                                Console.WriteLine(kli.Id + " --- " + kli.Imie + " - " + kli.Nazwisko);
                            }
                            String klientId = Console.ReadLine();
                            Klient klient = klientRepo.FindOne(klientId);
                            if (klient == null)
                                Console.WriteLine("Błędny numer ID");
                            else
                            {
                                Console.WriteLine("Podawaj id oferty do sprzedania");
                                List<Oferta> oferty = ofertaKlientRepo.FindAllAvailable();
                                if (oferty.Count == 0)
                                {
                                    Console.WriteLine("Brak ofert");
                                    continue;
                                }
                                foreach (Oferta el in oferty)
                                {
                                    string dostepna = "";
                                    if (el.LiczbaOfert > 0)
                                        dostepna = "dostępna";
                                    else
                                        dostepna = "niedostępna";
                                    Console.WriteLine(el.Id + " --- " + el.Nazwa + " - " + el.Panstwo + " - " + el.Miejscowosc + "-" + el.DniPobytu + " (" + dostepna + ")");
                                }
                                String ofertaId = Console.ReadLine();
                                Oferta oferta = ofertaRepo.FindOne(ofertaId);
                                if (oferta == null)
                                {
                                    Console.WriteLine("Błędny numer ID");
                                    continue;
                                }
                                Console.WriteLine("Podaj date przybycia (w formacie mm/dd/yyyy) ");
                                String data = Console.ReadLine();
                                OfertaKlient sprzedaz = new OfertaKlient(Guid.Parse(ofertaId), Guid.Parse(klientId), DateTime.Parse(data), DateTime.Parse(data).AddDays(oferta.DniPobytu));
                                if (ofertaKlientRepo.Save(sprzedaz, oferta) == true)
                                    Console.WriteLine("Wypozyczono !");
                                else
                                    Console.WriteLine("Błąd wypożyczenia");
                            }
                        }
                        else if (key.Equals(ConsoleKey.D2))
                        {
                            Console.WriteLine("Podaj ID klienta: ");
                            List<Klient> klienci = klientRepo.FindAll();
                            foreach (Klient kli in klienci)
                            {
                                Console.WriteLine(kli.Id + " --- " + kli.Imie + " - " + kli.Nazwisko);
                            }
                            String klientId = Console.ReadLine();
                            Klient klient = klientRepo.FindOne(klientId);
                            if (klient == null)
                                Console.WriteLine("Błędny numer ID");
                            else
                            {
                                List<OfertaKlient> sprzedaze = ofertaKlientRepo.FindAll(klientId);
                                Console.WriteLine("Wypożyczenia " + klient.Imie + " - " + klient.Nazwisko + " :");
                                foreach (OfertaKlient sprzedaz in sprzedaze)
                                {
                                    Oferta sprzedana = ofertaRepo.FindOne(sprzedaz.IdOferta.ToString());
                                    if (sprzedaz.Zakonczenie.CompareTo(DateTime.Now) > 0)
                                    {
                                        Console.WriteLine("[Niezrealizowane] --- " + sprzedana.Nazwa + " - " + sprzedana.Panstwo + " - " + sprzedana.Miejscowosc + " - " + sprzedana.DniPobytu);
                                    }
                                    else
                                    {
                                        Console.WriteLine(sprzedana.Nazwa + " - " + sprzedana.Panstwo + " - " + sprzedana.Miejscowosc + " - " + sprzedana.DniPobytu);
                                    }
                                }
                            }
                        }
                        else if (key.Equals(ConsoleKey.D3))
                        {
                            List<Oferta> oferty = ofertaKlientRepo.FindAllAvailable();
                            if (oferty.Count == 0)
                            {
                                Console.WriteLine("Brak książek");
                            }
                            foreach (Oferta oferta in oferty)
                            {
                                Console.WriteLine(oferta.Id + " --- " + oferta.Nazwa + " - " + oferta.Panstwo + " - " + oferta.Miejscowosc + "-" + oferta.DniPobytu);
                            }
                        }
                        else if (key.Equals(ConsoleKey.Q))
                        {
                            break;
                        }
                    }
                }
                else if (key.Equals(ConsoleKey.D4))
                {
                    inicjalizacja.DropTables(session);
                }
                else if (key.Equals(ConsoleKey.Q))
                {
                    break;
                }
            }
        }
    }
}
