﻿using Cassandra.Mapping;
using MyCassandra.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCassandra
{
    public class MyMappings : Mappings
    {
        public MyMappings()
        {
            For<Oferta>().TableName("Oferta").PartitionKey(o => o.Id);
            For<OfertaKlient>().TableName("OfertaKlient").PartitionKey(o => o.Id);
        }
    }
}
