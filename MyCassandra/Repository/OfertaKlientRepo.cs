﻿using Cassandra;
using Cassandra.Data.Linq;
using MyCassandra.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCassandra.Repository
{
    public class OfertaKlientRepo
    {
        public ISession Session;

        public OfertaKlientRepo(ISession session)
        {
            this.Session = session;
        }

        public List<OfertaKlient> FindAll(String id)
        {
            var oferty = new Table<OfertaKlient>(Session);

            List<OfertaKlient> ofertyList = oferty.Execute().Where(o => o.IdKlient == Guid.Parse(id)).ToList();
            return ofertyList;

        }
        public List<Oferta> FindAllAvailable()
        {
            var oferty = new Table<Oferta>(Session);

            List<Oferta> ofertyList = oferty.Execute().Where(o => o.LiczbaOfert > 0).ToList();
            return ofertyList;
        }
        public bool Save(OfertaKlient ofertaK, Oferta oferta)
        {
            try
            {
                var ofertyK = new Table<OfertaKlient>(Session);
                ofertaK.Id = Guid.NewGuid();
                ofertyK.Insert(ofertaK).Execute();

                var oferty = new Table<Oferta>(Session);

                oferty.Where(u => u.Id == ofertaK.IdOferta).Select(u => new Oferta { LiczbaOfert = oferta.LiczbaOfert - 1  }).Update().Execute();
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }
    }
}
