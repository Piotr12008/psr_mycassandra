﻿using Cassandra;
using Cassandra.Data.Linq;
using Cassandra.Mapping;
using MyCassandra.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCassandra.Repository
{
    public class OfertaRepo
    {
        public ISession Session;

        public OfertaRepo(ISession session)
        {
            this.Session = session;
        }
        public List<Oferta> FindAll()
        {
            var oferty = new Table<Oferta>(Session);

            List<Oferta> ofertyList = oferty.Execute().ToList();
            return ofertyList;
        }
        public Oferta FindOne(String id)
        {
            var ofertaO = new Table<Oferta>(Session).Where(o => o.Id == Guid.Parse(id)).First();
            Oferta oferta = ofertaO.Execute();

            return oferta;
        }
        public List<Oferta> FindAll(String panstwo)
        {
            var ofertaO = new Table<Oferta>(Session);
            List<Oferta> oferty = ofertaO.Execute().Where(o => o.Panstwo.Equals(panstwo)).ToList();

            return oferty;
        }

        public bool Save(Oferta oferta)
        {
            try
            {
                var oferty = new Table<Oferta>(Session);
                oferta.Id = Guid.NewGuid();
                oferty.Insert(oferta).Execute();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public bool Remove(String id)
        {
            try
            {
                var oferty = new Table<Oferta>(Session);
                oferty.Where(u => u.Id == Guid.Parse(id)).Delete().Execute();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
