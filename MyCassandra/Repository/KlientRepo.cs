﻿using Cassandra;
using MyCassandra.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCassandra.Repository
{
    public class KlientRepo
    {
        public ISession Session;

        public KlientRepo(ISession session)
        {
            this.Session = session;
        }
        public List<Klient> FindAll()
        {
            var results = Session.Execute("SELECT * FROM Klient;");
            List<Klient> klienci = new List<Klient>();
            foreach (var result in results)
            {
                Klient klient = new Klient();
                klient.Id = result.GetValue<Guid>("id");
                klient.Imie = result.GetValue<string>("imie").ToString();
                klient.Nazwisko = result.GetValue<string>("nazwisko").ToString();
                klienci.Add(klient);
            }

            return klienci;
        }

        public Klient FindOne(String id)
        {
            var result = Session.Execute("SELECT * FROM Klient WHERE id = " + Guid.Parse(id) + ";").First();

            Klient klient = new Klient();
            klient.Id = result.GetValue<Guid>("id");
            klient.Imie = result.GetValue<string>("imie").ToString();
            klient.Nazwisko = result.GetValue<string>("nazwisko").ToString();

            return klient;
        }

        public bool Save(Klient klient)
        {
            try
            {
                Session.Execute("INSERT INTO Klient (Id, Imie, Nazwisko) VALUES (uuid(), '" + klient.Imie + "', '" + klient.Nazwisko + "' )");
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public bool Update(Klient klient)
        {
            try
            {
                Session.Execute("UPDATE Klient SET Imie = '" + klient.Imie + "', Nazwisko = '" + klient.Nazwisko + "' WHERE Id = " + klient.Id + ";");
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool Remove(String id)
        {
            try
            {
                Session.Execute("DELETE FROM Klient WHERE id = " + id + ";");
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
