﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCassandra.Entity
{
    public class OfertaKlient
    {
        public Guid Id { get; set; }
        public Guid IdOferta { get; set; }
        public Guid IdKlient { get; set; }
        public DateTime Rozpoczecie { get; set; }
        public DateTime Zakonczenie { get; set; }

        public OfertaKlient(Guid idOferta, Guid idKlient, DateTime rozpoczecie, DateTime zakonczenie)
        {
            this.IdOferta = idOferta;
            this.IdKlient = idKlient;
            this.Rozpoczecie = rozpoczecie;
            this.Zakonczenie = zakonczenie;
        }

        public OfertaKlient() { }
    }
}
