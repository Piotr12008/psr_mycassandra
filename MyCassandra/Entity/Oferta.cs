﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCassandra.Entity
{
    public class Oferta
    {
        public Guid Id { get; set; }
        public String Nazwa { get; set; }
        public String Panstwo { get; set; }
        public String Miejscowosc { get; set; }
        public int DniPobytu { get; set; }
        public int LiczbaOfert { get; set; }

        public Oferta(String nazwa, String panstwo, String miejscowosc, int dniPobytu, int liczbaOfert)
        {
            this.Nazwa = nazwa;
            this.Panstwo = panstwo;
            this.Miejscowosc = miejscowosc;
            this.DniPobytu = dniPobytu;
            this.LiczbaOfert = liczbaOfert;
        }
        public Oferta() { }
    }
}
