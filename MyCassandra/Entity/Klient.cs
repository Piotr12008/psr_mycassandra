﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyCassandra.Entity
{
    public class Klient
    {
        public Guid Id { get; set; }
        public String Imie { get; set; }
        public String Nazwisko { get; set; }

        public Klient()
        {
        }

        public Klient(String imie, String nazwisko)
        {
            this.Imie = imie;
            this.Nazwisko = nazwisko;
        }
    }
}
